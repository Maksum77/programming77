﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using logic;

namespace logic
{
    public static class CounterWeight
    {
        public static int CountWeight(Present gift)
        {
            int Sum = 0;

            foreach (var candy in gift.Gift)
            {
                Sum = Sum+ candy.Weight;
                
            }
            return Sum;
        }
    }
}
